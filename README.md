# phosh-convergence-buddy

Quick settings for phosh.  Only tested on archlinux-arm on pinephone.  Other distros may require changes.

Based on scale_screen: 
- https://wp.puri.sm/posts/easy-librem-5-app-development-scale-the-screen/
- https://source.puri.sm/librem5-apps/librem5-goodies/-/blob/master/l5-scale-the-screen


Depends:
- wlr-randr
- yad

Optional:
- phosh-flexed
- phoc
- squeekboard
- systemd


Features:
- Quick settings to adjust rotation and scaling for touch and docked mode
- Quick rotation settings
- Kill sensors (prox, accel)
- Kill squeekboard
- Workarounds for bugs in phosh-flexed

Installation:
Copy script to /usr/bin and mark it as executable.
Copy .desktop to /usr/share/applications.
